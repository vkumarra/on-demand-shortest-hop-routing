
CC = gcc

FLAGS = -g -02

LIBS =  /home/users/cse533/Stevens/unpv13e/libunp.a

# take care of this hard coded include path
CFLAGS = ${FLAGS}
FLAGS = -g -O2

all: odr.o get_hw_addrs.o server.o client.o api.o
	${CC} -o ODR_vrai odr.o get_hw_addrs.o ${LIBS}
	${CC} -o server_vrai server.o api.o  ${LIBS}
	${CC} -o client_vrai client.o api.o ${LIBS}

get_hw_addrs.o: get_hw_addrs.c
	${CC} ${FLAGS} -c get_hw_addrs.c

odr.o: odr.c
	${CC} ${FLAGS} -c odr.c

server.o: server.c
	${CC} ${FLAGS} -c server.c

client.o: client.c
	${CC} ${FLAGS} -c client.c

api.o: api.c
	${CC} ${FLAGS} -c api.c

clean:
	rm ODR_vrai odr.o get_hw_addrs.o api.o client_vrai client.o server_vrai server.o

