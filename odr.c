#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <sys/un.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "headers.h"

static struct hwa_info *hwa = NULL;
static struct sockaddr_in *self_addr = NULL;
static struct bid_struct *bid_check_list = NULL;
static struct route_entry *route_table = NULL;
static uint64_t stale_time;
static struct pending_frame *pending_frame_list = NULL;
static struct port_mapping *port_mapping_list = NULL;
uint16_t global_port_num = 1000;
int unx_sock;

int send_packet(int sock, int src_idx, uint8_t dest_mac[6], uint8_t frame_type);
void print_packet_info(char unsigned *buf);

int main(int argc, char *argv[])
{
	
	int pf_sock, j, send_result;
	unsigned char src_mac[6], dest_mac[6];	
	uint8_t ip_addr[4], dest_ip_addr;
	unsigned char *buf;
	struct sockaddr_un odr_addr;
	char name[20];
	struct port_mapping server_entry;
	struct timeval tv;
	long curr_time;
	int length;
	

	if (argc != 2) {
		printf("Usage %s stale_value\n", argv[0]);
		return 1;
	}

	stale_time = atoi(argv[1]);
	hwa = Get_hw_addrs();
	gettimeofday(&tv, NULL);
	curr_time = tv.tv_sec;

	/* Unix domain socket */	
	unlink(ODR_PATH);
	unx_sock = socket(AF_LOCAL, SOCK_DGRAM, 0);
	
	bzero(&odr_addr, sizeof(odr_addr));
	odr_addr.sun_family = AF_LOCAL;
	strcpy(odr_addr.sun_path, ODR_PATH);

	length = sizeof(odr_addr);
	if (bind(unx_sock, (struct sockaddr *)&odr_addr, sizeof(odr_addr)) < 0) {
		printf("Error in binding unix domain socket\n");
		return -1;
	}

	/* PF_PACKET socket */	
	pf_sock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if (pf_sock < 0) {
		printf("Error in getting pf socket descriptor\n");
	}
	
	server_entry.port = 21340;
	strcpy(server_entry.file_name, SERVER_PATH);
	server_entry.time_stamp = curr_time; 
	server_entry.next = NULL;
	port_mapping_list = &server_entry;

	print_ip_addr();
	buf = malloc(ETH_FRAME_LEN);
	get_ip_addr(ip_addr);

	pkt_process(pf_sock, unx_sock);

	return 1;
}

int print_ip_addr() {

	struct hwa_info *hwa_list;
	char str[128];
	struct sockaddr_in *sin;
	
	hwa_list = hwa;
	hwa_list = hwa_list->hwa_next;
	
	sin = (struct sockaddr_in *)(hwa_list->ip_addr);
	if (inet_ntop(AF_INET, &sin->sin_addr, str, sizeof(str)) == NULL) {
		printf("Error reading IP address\n");
	} else {
		printf("IP address: %s\n", str);
	}

}

int print_address(uint8_t *addr) {
	
	int i;

	for (i=0;i<4;i++) {
		printf("%d", addr[i]);
		if (i<3) printf(".");
	}
}

int print_mac_address(uint8_t *addr) {

	int i;
	for (i=0;i<6;i++) {
		printf("%02x", addr[i]);
		if (i<5) printf(":");
	}
	
}

int get_ip_addr(uint8_t *addr) {
	
	struct hwa_info *hwa_list;
	struct sockaddr_in *sin;
	uint8_t *start_addr;
	
	hwa_list = hwa;
	hwa_list = hwa_list->hwa_next;

	sin = (struct sockaddr_in *)(hwa_list->ip_addr);
	start_addr = (uint8_t *) &(sin->sin_addr.s_addr);
	memcpy(addr, start_addr, 4);
}

int pkt_process(int pf_socket, int ud_socket) {

	fd_set rset;
	struct sockaddr_ll socket_address;
	struct sockaddr_un unx_socket_address;
	unsigned char *buf;
	int len, recv_len,i;
	struct packet_elements pkt_elem;
	uint8_t pkt_type;
	int maxfd;

	buf = malloc(ETH_FRAME_LEN);
	len = sizeof(struct sockaddr_ll);	

	FD_ZERO(&rset);
	
	while (1) {
		FD_SET(pf_socket, &rset);
		FD_SET(ud_socket, &rset);

		if (pf_socket > ud_socket) {
			maxfd = pf_socket + 1;
		} else {
			maxfd = ud_socket + 1;
		}

		select(maxfd, &rset, NULL, NULL, NULL);
		if (FD_ISSET(pf_socket, &rset)) {
			recv_len = recvfrom(pf_socket, buf, ETH_FRAME_LEN, 0, (struct sockaddr *)&socket_address, &len);
			if (recv_len < 0) {
				printf("Error in reading data from pf socket\n");
			}

			if (ntohs(socket_address.sll_protocol) == PROTOCOL_NUMBER) {
				pkt_type = *((unsigned char *)buf + 14);
				recv_print();
				print_packet_info(buf);	
				switch(pkt_type) {
					case RREQ_PKT:
					rreq_packet_process(buf, &socket_address, pf_socket);
					break;
					case RREP_PKT:
					rrep_packet_process(buf, &socket_address, pf_socket);
					break;
					case DATA_PKT:
					data_packet_process(buf, &socket_address, pf_socket);
					break;
					default:
					printf("Invalid packet\n");
				}
			}
		}

		if (FD_ISSET(ud_socket, &rset)) {
			recv_len = recvfrom(ud_socket, buf, sizeof(struct data_packet), 0, (struct sockaddr *)&unx_socket_address, &len);
			
			if (recv_len < 0) {
				printf("Error in reading data from unix domain socket\n");
			}			
			send_data_packet(pf_socket, buf, unx_socket_address.sun_path);			
		}
	}
}

int send_data_packet(int sock, unsigned char *buf, char *file_name) {

	struct data_packet *src_data_pkt, *dst_data_pkt;
	struct odr_frame *odr_frm;
	unsigned char *frame_buf;
	uint8_t src_ip_addr[4];
	struct route_entry *rt;
	long curr_time;
	struct timeval tv;
	struct ethhdr *eh; 
	struct route_entry *rt_entry;

	gettimeofday(&tv, NULL);
	curr_time = tv.tv_sec;


	frame_buf = (unsigned char*)malloc(ETH_FRAME_LEN);
	
	eh = (struct ethhdr *)frame_buf;
	odr_frm = (struct odr_frame *)(frame_buf + 14);
	odr_frm->pkt_type = DATA_PKT;
	dst_data_pkt = &(odr_frm->u_field.dt_pkt);
	get_ip_addr(src_ip_addr);

	src_data_pkt = (struct data_packet *)buf;
	
	memcpy(dst_data_pkt, src_data_pkt, sizeof(struct data_packet));
	memcpy(dst_data_pkt->src_ip_addr, src_ip_addr, 4);	
	eh->h_proto = htons(PROTOCOL_NUMBER);	
	dst_data_pkt->hop_cnt = 0;

	if (file2port_lookup(port_mapping_list, &(dst_data_pkt->tpkt.src_port), file_name) < 0) {
		dst_data_pkt->tpkt.src_port = port_table_entry(&port_mapping_list, file_name);
	} 
	
	rt_entry = lookup(route_table, dst_data_pkt->dst_ip_addr);

	forced_rediscovery_check(rt_entry, dst_data_pkt->force_discov);
	
	if ((rt_entry == NULL) || (curr_time > rt_entry->time_stamp + stale_time)) {
		enqueue_frame(&pending_frame_list, frame_buf);
		send_rreq(sock, dst_data_pkt->dst_ip_addr, -1, dst_data_pkt->force_discov);
	} else {
		fwd_pkt(sock, frame_buf, rt_entry->if_index, rt_entry->mac_addr);
	}

	free(frame_buf);
}

int port2file_lookup(struct port_mapping *prt_map, uint16_t port_num, char *file_name) {

	while ((prt_map  != NULL) && (prt_map->port != port_num)) {
		prt_map = prt_map->next;
	}

	if (prt_map == NULL) {
		return -1;
	} else {
		strcpy(file_name, prt_map->file_name);
		return 1;
	}
}
 
int file2port_lookup(struct port_mapping *prt_map, uint16_t *port_num, char *file) {

	while ((prt_map != NULL) && (strcmp(prt_map->file_name, file) != 0)) {
		prt_map = prt_map->next;
	}

	if (prt_map == NULL) {
		return -1;
	} else {
		*port_num = prt_map->port;
		return 1;
	}
}

int port_table_entry(struct port_mapping **prt_map, char *file_name) {
	
	struct port_mapping *temp_map, *new_entry;
	struct timeval tv;
	long curr_time;

	gettimeofday(&tv, NULL);
	curr_time = tv.tv_sec;

	temp_map = *prt_map;
	if (temp_map != NULL) {
		while (temp_map->next != NULL) {
			temp_map = temp_map->next;
		}
	}

	global_port_num++;
	new_entry = (struct port_mapping *)malloc(sizeof(struct port_mapping));
	new_entry->port = global_port_num;
	strcpy(new_entry->file_name, file_name);
	new_entry->time_stamp = curr_time;
	new_entry->next = NULL;

	if (*prt_map == NULL) {
		*prt_map = new_entry;
	} else {
		temp_map->next = new_entry;	
	}
	
	return new_entry->port;
}

int process_data(unsigned char *buf) {

	struct odr_frame *odr_frm;
	struct data_packet *data_pkt;
	unsigned char *final_data;
	unsigned char *source;
	uint16_t port_num;
	char file_name[50];
	struct sockaddr_un addr;
	int bytes_sent;
	int ret_val;

	final_data = (unsigned char *)malloc(sizeof(struct data_packet));
	odr_frm = (struct odr_frame *)(buf + 14);
	data_pkt = &(odr_frm->u_field.dt_pkt);
	source = (unsigned char *)data_pkt;
	memcpy(final_data, source, sizeof(struct data_packet));

	port_num = data_pkt->tpkt.dst_port;
	ret_val = port2file_lookup(port_mapping_list, port_num, file_name);

	if (ret_val < 0) {
		printf("Error looking up port number\n");
	}

	bzero(&addr, sizeof(addr));
	addr.sun_family = AF_LOCAL;
	strcpy(addr.sun_path, file_name);

	bytes_sent = sendto(unx_sock, final_data, sizeof(struct data_packet), 0, (struct sockaddr *)&addr, sizeof(addr));	
	if (bytes_sent < 0) {
		printf("Error writing data to unix domain socket\n");
	}
}

int rreq_packet_process(unsigned char *buf, struct sockaddr_ll *socket_address, int sock) {

	struct rreq_packet *rreq_pkt;
	struct odr_frame *odr_frm;
	uint8_t self_ip_addr[4];
	struct route_entry *rt_entry;
	int better_route;
	struct timeval tv;
	long curr_time;
	int i;

	odr_frm = (struct odr_frame *)(buf + 14);
	rreq_pkt = &(odr_frm->u_field.rq_pkt);

	gettimeofday(&tv, NULL);
	curr_time = tv.tv_sec;

	get_ip_addr(self_ip_addr);

	rt_entry = lookup(route_table, rreq_pkt->src_ip_addr);

	/* If there is no route entry for the source ip address, it implies
	   that its the first RREQ from that source, simply create a new
	   entry for this source ip address in the bid check list, else
	   this the node has received RREQ from the source before, check
	   if its duplicate RREQ and if duplicate has it got better path 
	*/

	if (rt_entry != NULL) {
		if (bcast_check(&bid_check_list, rreq_pkt->bid, rreq_pkt->src_ip_addr) == 0) {
			if ((rt_entry->hop_count < rreq_pkt->hop_cnt + 1) && 
			    (curr_time < rt_entry->time_stamp + stale_time)) {
				return 1;	
			}
		}
	} else {
		bcast_check(&bid_check_list, rreq_pkt->bid, rreq_pkt->src_ip_addr);
	}

	rreq_pkt->hop_cnt++;

	forced_rediscovery_check(rt_entry, rreq_pkt->force_discov);
	/* Check if you can reply for this request

	   There will be following scenarios:
	   1) your ip and dest ip of the RREQ are same
	   2) your ip and dest ip of the RREQ are different
	 
	   Response:
	   1) check if you have entry for this dest in routing table, if yes send out unicast RREP 
	      packet out of incoming interface and routing entry for that ip. In case there is no
	      entry for the dest ip in the routing table, add an entry for that ip in the routing
	      table, send unicast RREP out on incoming interface. Update the routing table with
	      reverse path. Frame a RREQ packet with no response falg set and broadcast out of all 
	      except the incoming interface.
	   2) check if you have entry for the dest ip in your routing table and its valid, in case 
	      there is an entry and its valid, form a RREP with that route and send out of incoming 
	      interface. In case you don't have an entry for the source ip or there is an entry but
	      it has become stale, in that case  send out RREQ out of all interfaces except incoming
	      interface and remove the stale entry from the routing table. Also update the routing
	      table with reverse path in both case.
	*/

	better_route = update_route_table(&route_table, rreq_pkt->src_ip_addr, rreq_pkt->hop_cnt,
			     		  socket_address->sll_ifindex, socket_address->sll_addr);

	if (rreq_pkt->no_redis == 1) {
		rreq_broadcast(buf, socket_address->sll_ifindex, sock);
		return 1;
	}

	if (comp(self_ip_addr, rreq_pkt->dst_ip_addr)) {  // you have to send out RREP and also check for better_route flag
	
		if (better_route == 1) {
			send_rrep_pkt(sock, buf, socket_address->sll_ifindex, socket_address->sll_addr, 0);
			rreq_pkt->no_redis = 1;
			rreq_pkt->force_discov = 0;
			rreq_broadcast(buf, socket_address->sll_ifindex, sock);
		}

	} else {
		rt_entry = lookup(route_table, rreq_pkt->dst_ip_addr);
		forced_rediscovery_check(rt_entry, rreq_pkt->force_discov);

		if ((rt_entry != NULL) && (curr_time <= rt_entry->time_stamp + stale_time)) {  
			
			if (better_route == 1) {
				send_rrep_pkt(sock, buf, socket_address->sll_ifindex, socket_address->sll_addr, 
					      rt_entry->hop_count);
				rreq_pkt->no_redis = 1;
				rreq_broadcast(buf, socket_address->sll_ifindex, sock);
			}
		} else {
			rreq_broadcast(buf, socket_address->sll_ifindex, sock);
		}
	}
}

int update_route_table(struct route_entry **rtable, uint8_t ip_addr[4], int hop_count, 
		uint8_t if_index, uint8_t *mac_addr) 
{

	struct route_entry *table = *rtable, *last = *rtable, *new_entry;
	struct timeval tv;
	long curr_time;
	int better_route = 0;
	
	while ((table != NULL) && (comp(ip_addr, table->ip_addr) == 0)) {
		table = table->next;
	}
	
	gettimeofday(&tv, NULL);
	curr_time = tv.tv_sec;
	
	if (*rtable != NULL) {
		while (last->next != NULL) {
			last = last->next;
		}
	}

	if (table == NULL) {
		new_entry = (struct route_entry *)malloc(sizeof(struct route_entry));
		memcpy(new_entry->ip_addr, ip_addr, 4);
		new_entry->hop_count = hop_count;
		new_entry->if_index = if_index;
		memcpy(new_entry->mac_addr, mac_addr, 6);
		new_entry->time_stamp = curr_time;
		new_entry->next = NULL;
		if (*rtable == NULL) {
			*rtable = new_entry;
		} else {
			last->next = new_entry;
		}
		better_route = 1;	
	} else {
		if ((table->hop_count > hop_count) || (curr_time > table->time_stamp + stale_time)) { 
			better_route = 1;
			table->hop_count = hop_count;
			table->if_index = if_index;
			table->time_stamp = curr_time;
			memcpy(table->mac_addr, mac_addr, 6);			
		} else if (table->hop_count == hop_count) {
			table->hop_count = hop_count;
			table->if_index = if_index;
			table->time_stamp = curr_time;
			memcpy(table->mac_addr, mac_addr, 6);		
		}
	}

	print_route_table(route_table);
	return better_route;
}

int rreq_broadcast(unsigned char *buf, int if_index, int sock) {

	unsigned char *data_buf;
	struct ethhdr *eh;
	struct sockaddr_ll socket_address;
	struct hwa_info *hwa_list;
	int send_result;

	data_buf = (unsigned char *)buf;
	eh = (struct ethhdr *)data_buf;
	hwa_list = hwa;

	while (hwa_list != NULL) {
		if ((hwa_list->if_index != if_index) &&
		    (strncmp(hwa_list->if_name, "lo", 2) != 0) &&
		    (strncmp(hwa_list->if_name, "eth0", 4) != 0) &&
		    (hwa_list->ip_alias != 1)) {

			memcpy(eh->h_source, hwa_list->if_haddr, 6);
			memset(socket_address.sll_addr, 0xff, 6);
			socket_address.sll_family = PF_PACKET;
			socket_address.sll_protocol = htons(PROTOCOL_NUMBER);
			socket_address.sll_ifindex = hwa_list->if_index;
			socket_address.sll_hatype = ARPHRD_ETHER;
			socket_address.sll_pkttype = PACKET_OTHERHOST;
			socket_address.sll_halen = ETH_ALEN;		

			send_print();
			print_packet_info(buf);	
			send_result = sendto(sock, buf, ETH_FRAME_LEN, 0,
                             (struct sockaddr*)&socket_address, sizeof(socket_address));
			if (send_result == -1) {
				printf("Error sending packet on interface %s\n", hwa_list->if_name);
			}
		}

		hwa_list = hwa_list->hwa_next;
	}


}

/* 
	Handling RREP packet
	There can be following two scenarios:
	1) Destination IP in RREP is different from your IP
	2) Destination IP in RREP is same as your IP

	Response
	1) The node checks if the route presented for the source
	IP in the RREP is better than existing entry. If yes
	go ahead and update that entry. In this scenarios RREP
	will be forwarded. To find the proper interface for
	forwarding a lookup will be performed in the routing table
	based on the destination IP address. If there is valid entry
	forward the RREP on that interface, else queue the RREP in pending
	packet list and send out RREQ for the destination. If the route
	presented for source IP by RREP is not better than the existing
	entry in routing table, that RREP is straight away disacrded.

	2) 
*/

int rrep_packet_process(unsigned char *buf, struct sockaddr_ll *socket_address, int sock) {

	uint8_t self_ip_addr[4];
	unsigned char *rrep_buf;
	struct odr_frame *odr_frm;
	struct rrep_packet *rrep_pkt;
	int better_route;
	struct route_entry *rt_entry;
	struct timeval tv;
	long curr_time;

	rrep_buf = malloc(ETH_FRAME_LEN);
	memcpy(rrep_buf, buf, ETH_FRAME_LEN);

	odr_frm = (struct odr_frame *)((unsigned char *)rrep_buf + 14);

	rrep_pkt = &(odr_frm->u_field.rp_pkt);
	rrep_pkt->hop_cnt++;

	rt_entry = lookup(route_table, rrep_pkt->src_ip_addr);	
	forced_rediscovery_check(rt_entry, rrep_pkt->force_discov);	
	better_route = update_route_table(&route_table, rrep_pkt->src_ip_addr, rrep_pkt->hop_cnt,
					  socket_address->sll_ifindex, socket_address->sll_addr);
	get_ip_addr(self_ip_addr);

	if (better_route == 0) {
		return 1;
	}

	gettimeofday(&tv, NULL);
	curr_time = tv.tv_sec;

	if (strncmp(self_ip_addr, rrep_pkt->dst_ip_addr, 4) == 0) {
		
		process_pending_frame(sock, &pending_frame_list);
	} else {
		rt_entry = lookup(route_table, rrep_pkt->dst_ip_addr);
	
		if ((rt_entry != NULL) && (rt_entry->time_stamp + stale_time >= curr_time)) {
			fwd_pkt(sock, rrep_buf, rt_entry->if_index, rt_entry->mac_addr);			
		} else {
			enqueue_frame(&pending_frame_list, buf);
			send_rreq(sock, rrep_pkt->dst_ip_addr, socket_address->sll_ifindex, rrep_pkt->force_discov);
		}

	}

	free(rrep_buf);
}

void process_pending_frame(int sock, struct pending_frame **frame_list) {

	struct pending_frame *temp_list = *frame_list, *temp;
	
	while (temp_list != NULL) {
		process_frame(sock, temp_list);
		temp_list = temp_list->next_frame;
	}

	temp_list = *frame_list;
	
	while (temp_list != NULL) {
		if (temp_list->processed_flag == 1) {
			temp = temp_list;
			if ((temp->prev_frame == NULL) && (temp->next_frame == NULL)){
				*frame_list = NULL;
			} else if (temp->prev_frame == NULL) {
				temp->next_frame->prev_frame = NULL;
				*frame_list = temp->next_frame;
			} else if (temp->next_frame == NULL) {
				temp->prev_frame->next_frame = NULL;
			} else {
				temp->next_frame->prev_frame = temp->prev_frame;
				temp->prev_frame->next_frame = temp->next_frame;
			}
			temp_list = temp_list->next_frame;
			free(temp);
		}
	}
}

void process_frame(int sock, struct pending_frame *pending_frm) {

	struct odr_frame *odr_frm;
	uint8_t dst_ip_addr[4];
	struct route_entry *rt_entry;
	struct timeval tv;
	long curr_time;

	gettimeofday(&tv, NULL);
	curr_time = tv.tv_sec;

	odr_frm = (struct odr_frame *)(pending_frm->frame + 14);
	
	if (odr_frm->pkt_type == RREP_PKT) {
		memcpy(dst_ip_addr, odr_frm->u_field.rp_pkt.dst_ip_addr, 4);
	} else if (odr_frm->pkt_type == DATA_PKT) {
		memcpy(dst_ip_addr, odr_frm->u_field.dt_pkt.dst_ip_addr, 4);
	}	

	rt_entry = lookup(route_table, dst_ip_addr);
	
	if ((rt_entry != NULL) && (curr_time <= rt_entry->time_stamp + stale_time)) {
		fwd_pkt(sock, pending_frm->frame, rt_entry->if_index, rt_entry->mac_addr);
		pending_frm->processed_flag = 1;
	}
}

void enqueue_frame(struct pending_frame **frame_list, unsigned char *buf) {

	struct pending_frame *temp_list = *frame_list;
	struct pending_frame *new_frame;

	new_frame = (struct pending_frame *)malloc(sizeof(struct pending_frame));
	new_frame->frame = (unsigned char *)malloc(ETH_FRAME_LEN);
	new_frame->processed_flag = 0;
	new_frame->next_frame = NULL;
	new_frame->prev_frame = NULL;
	
	memcpy(new_frame->frame, buf, ETH_FRAME_LEN);

	if (temp_list == NULL) {
		*frame_list = new_frame;
	} else {
		while (temp_list->next_frame != NULL) {
			temp_list = temp_list->next_frame;
		}
		temp_list->next_frame = new_frame;
		new_frame->prev_frame = temp_list;
	}
}


int data_packet_process(unsigned char *buf, struct sockaddr_ll *socket_address, int sock) {

	uint8_t self_ip_addr[4];
	struct route_entry *rt_entry;
	struct odr_frame *frame;
	struct data_packet *data_pkt;
	struct timeval tv;
	long curr_time;
	struct pending_frame *p_frame;

	frame = (struct odr_frame *)(buf + 14);
	data_pkt = &(frame->u_field.dt_pkt);

	get_ip_addr(self_ip_addr);
	gettimeofday(&tv, NULL);

	curr_time = tv.tv_sec;
	data_pkt->hop_cnt++;
	update_route_table(&route_table, data_pkt->src_ip_addr, data_pkt->hop_cnt,
                           socket_address->sll_ifindex, socket_address->sll_addr);

	if (strncmp(self_ip_addr, data_pkt->dst_ip_addr, 4) == 0) {
		
		process_data(buf);
	} else {

		rt_entry = lookup(route_table, data_pkt->dst_ip_addr);

		if ((rt_entry != NULL) && (curr_time <= rt_entry->time_stamp + stale_time)) {
			fwd_pkt(sock, buf, rt_entry->if_index, rt_entry->mac_addr);
		} else {
			enqueue_frame(&pending_frame_list, buf);
			send_rreq(sock, data_pkt->dst_ip_addr, socket_address->sll_ifindex, 0);
		}

	}
}

int process_header(unsigned char *buf, struct packet_elements *pkt_elem) {
	
	unsigned char *header;
	int i;
	header = buf;

	memcpy(pkt_elem->dst_mac, header, 6);
	memcpy(pkt_elem->src_mac, header+6, 6);

	memcpy(pkt_elem->dst_ip_addr, header + 15, 4);
	memcpy(pkt_elem->src_ip_addr, header + 19, 4);

	for (i=0;i<22;i++) {
		printf("%02x:", header[i]);
	}
}

/*
int send_packet(int sock, int src_idx, uint8_t dest_mac[6], uint8_t frame_type) {

	struct sockaddr_ll socket_address;
	unsigned char *buf;
	unsigned char *eth_header, *data, *odr_section;
	struct ethhdr *eh;
	int i, send_result;
	uint8_t src_mac[6];	
	struct hwa_info *hwa_list;
	struct odr_frame *odr_header;
	uint8_t dest_ip_addr[4];

 	hwa_list = hwa;
	buf = (unsigned char *)malloc(ETH_FRAME_LEN);
	eth_header = buf;
	data = buf + 14;
	eh = (struct ethhdr *)eth_header;	
	odr_section = data;
	odr_header = (struct odr_frame *)odr_section;
	
	// Send only RREQ only for the time being 
	odr_header->pkt_type = RREQ_PKT;

	dest_ip_addr[0] = 0xc0;
	dest_ip_addr[1] = 0xa8;
	dest_ip_addr[2] = 0x01;
	dest_ip_addr[3] = 0x6a;

	send_rreq_pkt(sock, buf, dest_ip_addr);
	while ((hwa_list != NULL) && (hwa_list->if_index != src_idx)) {
		hwa_list = hwa_list->hwa_next;
	}

	if (hwa_list == NULL) {
		printf("Invalid interface index\n");
		return -1;
	}


	memcpy(src_mac, hwa_list->if_haddr, 6);
	memcpy(socket_address.sll_addr, dest_mac, 6);

	socket_address.sll_family = PF_PACKET;
	socket_address.sll_protocol = htons(PROTOCOL_NUMBER);
	socket_address.sll_ifindex = src_idx;
	socket_address.sll_hatype = ARPHRD_ETHER;
	socket_address.sll_pkttype = frame_type;
	socket_address.sll_halen = ETH_ALEN;

	//MAC - end
	socket_address.sll_addr[6]  = 0x00;
	socket_address.sll_addr[7]  = 0x00;	


	printf("Interface index used %d\n", hwa_list->if_index);
	printf("Source MAC address\n");
	for (i=0;i<6;i++) {
		printf("%02x:", src_mac[i]);
	}
	printf("\n");

	printf("Destination MAC address\n");
        for (i=0;i<6;i++) {
                printf("%02x:", dest_mac[i]);
        }
        printf("\n");

	printf("Frame type %d\n", frame_type); 
	memcpy((void *)buf, (void *)dest_mac, ETH_ALEN);
	memcpy((void *)(buf + ETH_ALEN), (void *)src_mac, ETH_ALEN);
	eh->h_proto = htons(PROTOCOL_NUMBER);

	send_print();
	print_packet_info(buf);
	send_result = sendto(sock, buf, ETH_FRAME_LEN, 0, 
			     (struct sockaddr*)&socket_address, sizeof(socket_address));

	if (send_result == -1) {
		printf("Error sending packer\n");
		return -1;
	}

	return 1;
}
*/

int send_rreq(int sock, uint8_t dst_ip_addr[4], int if_index, uint8_t force_discovery) {
	
	uint8_t self_ip_addr[4];
	unsigned char *rreq_buf;
	struct odr_frame *odr_frm;
	struct rreq_packet *rreq_pkt;

	struct hwa_info *hwa_list = hwa;
	struct sockaddr_ll socket_address;
	struct ethhdr *eh;
	int send_result;

	rreq_buf = (unsigned char *)malloc(ETH_FRAME_LEN);	

	eh = (struct ethhdr *)rreq_buf;
	odr_frm = (struct odr_frame *)(rreq_buf + 14);
	rreq_pkt = &(odr_frm->u_field.rq_pkt);

	get_ip_addr(self_ip_addr);
	memcpy(rreq_pkt->src_ip_addr, self_ip_addr, 4);
	memcpy(rreq_pkt->dst_ip_addr, dst_ip_addr, 4);
	
	odr_frm->pkt_type = RREQ_PKT;
	rreq_pkt->bid = next_bid(&bid_check_list); 
	rreq_pkt->hop_cnt = 0;
	rreq_pkt->no_redis = 0;
	rreq_pkt->force_discov = force_discovery;

	eh->h_proto = htons(PROTOCOL_NUMBER);

	if (if_index < 0) {

		while (hwa_list != NULL) {
			if ((strncmp(hwa_list->if_name, "lo", 2) != 0) &&
			    (strncmp(hwa_list->if_name, "eth0", 4) != 0) &&
			    (hwa_list->ip_alias != 1)) {
	
				memcpy(eh->h_source, hwa_list->if_haddr, 6);
				memset(eh->h_dest, 0xff, 6);
				memset(socket_address.sll_addr, 0xff, 6);			
				socket_address.sll_family = PF_PACKET;
				socket_address.sll_protocol = htons(PROTOCOL_NUMBER);
				socket_address.sll_ifindex = hwa_list->if_index;
				socket_address.sll_hatype = ARPHRD_ETHER;
				socket_address.sll_pkttype = PACKET_OTHERHOST;
				socket_address.sll_halen = ETH_ALEN;
				
				send_print();
				print_packet_info(rreq_buf);
				send_result = sendto(sock, rreq_buf, ETH_FRAME_LEN, 0,
					(struct sockaddr*)&socket_address, sizeof(socket_address));
				if (send_result == -1) {
                         	       printf("Error sending packet on interface %s\n", hwa_list->if_name);
                        	} 
					
			}
			hwa_list = hwa_list->hwa_next;
		}

	} else {
		while (hwa_list != NULL) {
			if ((hwa_list->if_index != if_index) &&
			    (strncmp(hwa_list->if_name, "lo", 2) != 0) &&
			    (strncmp(hwa_list->if_name, "eth0", 4) != 0) &&
			    (hwa_list->ip_alias != 1)) {
	
				memcpy(eh->h_source, hwa_list->if_haddr, 6);
				memset(eh->h_dest, 0xff, 6);
				memset(socket_address.sll_addr, 0xff, 6);			
				socket_address.sll_family = PF_PACKET;
				socket_address.sll_protocol = htons(PROTOCOL_NUMBER);
				socket_address.sll_ifindex = hwa_list->if_index;
				socket_address.sll_hatype = ARPHRD_ETHER;
				socket_address.sll_pkttype = PACKET_OTHERHOST;
				socket_address.sll_halen = ETH_ALEN;
			
				send_print();	
				print_packet_info(rreq_buf);
				send_result = sendto(sock, rreq_buf, ETH_FRAME_LEN, 0,
					(struct sockaddr*)&socket_address, sizeof(socket_address));
				if (send_result == -1) {
                         	       printf("Error sending packet on interface %s\n", hwa_list->if_name);
                        	}
				
			}
			hwa_list = hwa_list->hwa_next;
		}
	}

	free(rreq_buf);	
}

/* This function needs to be removed */

int send_rreq_pkt(int sock, unsigned char *buf, uint8_t dst_ip_addr[4]) {
	
	unsigned char *rreq_section;
	struct odr_frame *odr_frm;
	struct rreq_packet *pkt;
	uint8_t src_ip_addr[4];
	uint8_t bid_ret_val;
	int i;

	odr_frm = (unsigned char *)(buf + 14);
	pkt = &(odr_frm->u_field.rq_pkt);

	get_ip_addr(src_ip_addr);	
	memcpy(pkt->dst_ip_addr, dst_ip_addr, 4);
	memcpy(pkt->src_ip_addr, src_ip_addr, 4);
	for(i=0;i<4;i++) {
		printf("%02x:", dst_ip_addr[i]);
	}
	printf("\n");
	for(i=0;i<4;i++) {
		printf("%02x:", pkt->dst_ip_addr[i]);
	}
	printf("\n");
	for(i=0;i<4;i++) {
		printf("%02x:", pkt->src_ip_addr[i]);
	}
	printf("\n");
	for(i=0;i<4;i++) {
		printf("%02x:", src_ip_addr[i]);
	}
	pkt->bid = 0x00;
	pkt->hop_cnt = 0x00;
	pkt->no_redis = 0x00;	

}

/* This function will create a brand new RREP packet  and forward it */


int send_rrep_pkt(int sock, unsigned char *buf, int if_index, uint8_t dst_mac_addr[6], uint8_t hop_count) {

	unsigned char *rrep_buf;
	struct odr_frame *out_frame;
	struct odr_frame *in_frame;
	struct rrep_packet *rrep_pkt;
	struct rreq_packet *rreq_pkt;
	struct sockaddr_ll socket_address;
	struct ethhdr *eh;
	int send_result;
	uint8_t src_mac_addr[6];
	struct hwa_info *hwa_struct;
	
	rrep_buf = (unsigned char *)malloc(ETH_FRAME_LEN);
	eh  = (struct ethhdr *)rrep_buf;
	out_frame = (struct odr_frame *)(rrep_buf + 14);
	in_frame = (struct odr_frame *)(buf + 14);
	rrep_pkt = &(out_frame->u_field.rp_pkt);
	rreq_pkt = &(in_frame->u_field.rq_pkt);

	out_frame->pkt_type = RREP_PKT;
	memcpy(rrep_pkt->src_ip_addr, rreq_pkt->dst_ip_addr, 4);
	memcpy(rrep_pkt->dst_ip_addr, rreq_pkt->src_ip_addr, 4);
	rrep_pkt->hop_cnt = hop_count;
	rrep_pkt->force_discov = rreq_pkt->force_discov;

	hwa_struct = hwa_data(if_index);

	memcpy(src_mac_addr, hwa_struct->if_haddr, 6);
	memcpy(socket_address.sll_addr, dst_mac_addr, 6);
	socket_address.sll_family = PF_PACKET;
	socket_address.sll_protocol = htons(PROTOCOL_NUMBER);
	socket_address.sll_ifindex = if_index;
	socket_address.sll_hatype = ARPHRD_ETHER;
	socket_address.sll_pkttype = PACKET_OTHERHOST;
	socket_address.sll_halen = ETH_ALEN;

	memcpy((void *)rrep_buf, (void *)dst_mac_addr, ETH_ALEN);
	memcpy((void *)(rrep_buf + ETH_ALEN), (void *)src_mac_addr, ETH_ALEN);
	eh->h_proto = htons(PROTOCOL_NUMBER);

	send_print();
	print_packet_info(rrep_buf);	
	send_result = sendto(sock, rrep_buf, ETH_FRAME_LEN, 0,
                             (struct sockaddr*)&socket_address, sizeof(socket_address));

	if (send_result == -1) {
		printf("Error sending RREP packet\n");
	}

	free(rrep_buf);
}

struct hwa_info  *hwa_data(int if_index) {

	struct hwa_info *hwa_list = hwa;
	
	while (hwa_list != NULL) {
		
		if ((hwa_list->if_index == if_index) && (hwa_list->ip_alias != 1)) {
			return hwa_list;
		}
		hwa_list = hwa_list->hwa_next;
	}
}

/* This function will forward a RREP/data packet */

int fwd_pkt(int sock, unsigned char *buf, int if_index, uint8_t dst_mac_addr[6]) {

	unsigned char *pkt_buf;
	uint8_t src_mac_addr[6];
	struct hwa_info *hwa_struct;	
	struct sockaddr_ll socket_address;
	int send_result;

	pkt_buf = (unsigned char *)malloc(ETH_FRAME_LEN);
	memcpy(pkt_buf, buf, ETH_FRAME_LEN);

	hwa_struct = hwa_data(if_index);

	memcpy(src_mac_addr, hwa_struct->if_haddr, 6);
	memcpy(socket_address.sll_addr, dst_mac_addr, 6);
	socket_address.sll_family = PF_PACKET;
	socket_address.sll_protocol = htons(PROTOCOL_NUMBER);
	socket_address.sll_ifindex = if_index;
	socket_address.sll_hatype = ARPHRD_ETHER;
	socket_address.sll_pkttype = PACKET_OTHERHOST;
	socket_address.sll_halen = ETH_ALEN;

	memcpy((void *)pkt_buf, (void *)dst_mac_addr, ETH_ALEN);
	memcpy((void *)(pkt_buf + ETH_ALEN), (void *)src_mac_addr, ETH_ALEN);

	send_print();
	print_packet_info(pkt_buf);
	send_result = sendto(sock, pkt_buf, ETH_FRAME_LEN, 0,
                             (struct sockaddr*)&socket_address, sizeof(socket_address));

	if (send_result == -1) {
		printf("Error forwarding packet\n");
	}

	free(pkt_buf);	
}

uint8_t next_bid(struct bid_struct **head) {
	
	struct bid_struct *temp = *head, *new_entry = NULL, *prev, *iter;
	uint8_t src_ip_addr[4];
	int null_flag;

	get_ip_addr(src_ip_addr);


	
	while ((temp != NULL) && (memcmp(temp->ip_addr, src_ip_addr, 4) != 0)) {
		temp = temp->next;
	}

	iter = *head;
	if (*head != NULL) {
		while (iter->next != NULL) {
			prev = iter;
			iter = iter->next;
		}
	}
			
	if (temp == NULL) {
		new_entry = (struct bid_struct *)malloc(sizeof(struct bid_struct));
		memcpy(new_entry->ip_addr, src_ip_addr, 4);
		new_entry->bid = 0x00;
		new_entry->next = NULL;

		if (*head == NULL) {
			*head = new_entry;
		} else {
			iter->next = new_entry;
		}
		return new_entry->bid;
	} else {
		temp->bid++;
		return temp->bid;
	}
}

int bcast_check(struct bid_struct **head, uint8_t bid, uint8_t ip_addr[4]) {

        struct bid_struct *temp = *head, *new_entry = NULL;
     
	while (temp != NULL) {
                if (comp(temp->ip_addr, ip_addr)) {
                        if (temp->bid >= bid) {
                                return FALSE;
                        } else {
                                temp->bid = bid;
                                return TRUE;
                        }
                } else {
                        temp = temp->next;
                }
        }

        new_entry = (struct bid_struct *)malloc(sizeof(struct bid_struct));
        memcpy(new_entry->ip_addr, ip_addr, 4);
        new_entry->bid = bid;
        new_entry->next = NULL;

        temp = *head;
        if (temp == NULL) {
                *head = new_entry;
        } else {
                while (temp->next != NULL) {
                        temp = temp->next;
                }
                temp->next = new_entry;
        }
	
	return TRUE;
}

struct route_entry *lookup(struct route_entry *head, uint8_t ip_addr[4]) {

        while (head != NULL) {
                if (comp(head->ip_addr, ip_addr)) {
                        return head;
                } else {
                        head = head->next;
                }
        }

        return head;
}

int comp(uint8_t *addr1, uint8_t *addr2) {

        if (memcmp(addr1, addr2, 4) == 0) {
                return TRUE;
        } else {
                return FALSE;
        }
}

void print_packet_info(unsigned char *buf) {

	struct odr_frame *frame;
        uint8_t pkt_type;

        frame = (struct odr_frame *)(buf + 14);
        pkt_type = frame->pkt_type;

        switch(pkt_type) {
                case RREQ_PKT:
                print_rreq_info(buf);
                break;
                case RREP_PKT:
                print_rrep_info(buf);
                break;
                case DATA_PKT:
                print_data_info(buf);
                break;
                default:
                printf("Invalid packet type\n");

        }
}

/* RREQ: ab:cd:ef:ab:cd:ef, ab:cd:ef:ab:cd:ef, 127.0.0.1, 127.0.0.1, bid, hop_count, redis_flag */ 
void print_rreq_info(unsigned char *buf) {

	struct odr_frame *frame;
	struct rreq_packet *rreq_pkt;
	
	frame = (struct odr_frame *)(buf + 14);
	rreq_pkt = &(frame->u_field.rq_pkt);

	printf("src ");
	print_mac_address(buf+6);
	printf(" dest ");
	print_mac_address(buf);
	printf("\nODR msg type 0 src ");
	print_host_name(rreq_pkt->src_ip_addr);
	printf(" dest ");
	print_host_name(rreq_pkt->dst_ip_addr);
	printf("\n");	
}

void print_rrep_info(unsigned char *buf) {
	
	struct odr_frame *frame;
        struct rrep_packet *rrep_pkt;

        frame = (struct odr_frame *)(buf + 14);
        rrep_pkt = &(frame->u_field.rp_pkt);

        printf("src ");
        print_mac_address(buf+6);
        printf(" dest ");
        print_mac_address(buf);
        printf("\nODR msg type 1 src ");
        print_host_name(rrep_pkt->src_ip_addr);
        printf(" dest ");
        print_host_name(rrep_pkt->dst_ip_addr);
        printf("\n");
}

void print_data_info(unsigned char *buf) {

	struct odr_frame *frame;
        struct data_packet *data_pkt;

        frame = (struct odr_frame *)(buf + 14);
        data_pkt = &(frame->u_field.dt_pkt);

        printf("src ");
        print_mac_address(buf+6);
        printf(" dest ");
        print_mac_address(buf);
        printf("\nODR msg type 2 src ");
        print_host_name(data_pkt->src_ip_addr);
        printf(" dest ");
        print_host_name(data_pkt->dst_ip_addr);
        printf("\n");
}


void print_route_table(struct route_entry *rt_entry) {
	
	printf("\n--------------Routing Table----------------\n");
	printf("Node 	MAC addr 		If Index 	Hop Count	Time Stamp\n");
	while (rt_entry != NULL) {
		print_route_entry(rt_entry);
		rt_entry = rt_entry->next;
	}
	printf("-------------------------------------------\n\n");
}

void print_route_entry(struct route_entry *rt_entry) {

	print_host_name(rt_entry->ip_addr);
	printf("	");
	print_mac_address(rt_entry->mac_addr);
	printf("	");
	printf("%d		", rt_entry->if_index);
	printf("%d		", rt_entry->hop_count);
	printf("%ld	", rt_entry->time_stamp);
	printf("\n"); 
}

int get_index(unsigned char *buf) {
	
	struct ethhdr *eh;
	uint8_t mac_addr[6];
	struct hwa_info *hwa_list;
	
	hwa_list = hwa;
	eh = (struct ethhdr *)buf;
	memcpy(mac_addr, eh->h_dest, 6);

	while (hwa_list != NULL) {
		if (memcmp(mac_addr, hwa_list->if_haddr, 6) == 0) {
			return hwa_list->if_index;
		}	
		hwa_list = hwa_list->hwa_next;
	}
}


int forced_rediscovery_check(struct route_entry *rt_entry, uint8_t rediscovery_flag) {

	if ((rt_entry != NULL) && (rediscovery_flag == 1)) {
		rt_entry->time_stamp = -1;
	}

}

void current_host_name() {

	uint8_t self_ip_addr[4];

	get_ip_addr(self_ip_addr);
	print_host_name(self_ip_addr);
}

void print_host_name(uint8_t *ip_addr) {

	struct hostent *hp = NULL;
	long print_ip_addr;

	memcpy(&print_ip_addr, ip_addr, 4);
	hp = gethostbyaddr((char *) &print_ip_addr, sizeof(print_ip_addr), AF_INET);
	printf("%s", hp->h_name);
}

void send_print() {
	printf("ODR at node ");
	current_host_name();
	printf(": sending frame hdr ");
}

void recv_print() {
	printf("ODR at node ");
        current_host_name();
        printf(": received frame hdr ");
}
